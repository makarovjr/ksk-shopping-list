window.addEventListener("DOMContentLoaded", () => {
  let shoppingList = JSON.parse(localStorage.getItem("shoppingList"));
  let fieldName = document.querySelector("[name=name]");
  let fieldQuantity = document.querySelector("[name=quantity]");
  let fieldPrice = document.querySelector("[name=price]");
  let fieldDate = document.querySelector("[name=purchaseDate]");

  // Запись списка покупок в localStorage

  function writeToLocalstorage(shoppingData) {
    localStorage.setItem("shoppingList", JSON.stringify(shoppingData));
  }

  // Сравнение имени с массивом

  function compareNameInShoppingList(shoppingData) {
    if (
      shoppingData.find((item) => item.name === fieldName.value) === undefined
    ) {
      return true;
    } else {
      return false;
    }
  }

  // Удаление элементов списка

  function deleteListItems() {
    document.querySelectorAll(".shopping__item").forEach((item) => {
      item.remove();
    });
  }

  // Заголовок таблицы

  function tableHeader() {
    deleteListItems();
    let tableHeaderName = {
      number: "№ п/п",
      name: "Наименование",
      quantity: "Кол-во",
      price: "Цена",
      data: "Дата",
      delete: "Удалить",
    };
    let li = document.createElement("li");
    li.classList.add("shopping__item");
    for (let key of Object.keys(tableHeaderName)) {
      let tableCell = document.createElement("div");
      tableCell.classList.add(
        `shopping__item-header_${tableHeaderName.key}`,
        "table-header",
        "shopping__item-cell"
      );
      tableCell.textContent = `${tableHeaderName[key]}`;
      li.append(tableCell);
    }
    document.querySelector(".shopping__list").append(li);
    console.log("ya tut");
  }

  // Отображение элементов списка

  function createListItems(shoppingData) {
    deleteListItems();
    tableHeader();
    for (let i = 0; i < shoppingData.length; i++) {
      let tableItem = shoppingData[i];
      let li = document.createElement("li");
      let index = document.createElement("div");
      let deleteButton = document.createElement("button");
      let btnContainer = document.createElement("div");
      let btnContent = document.createElement("span");
      li.classList.add(`shopping__item${i}`, "shopping__item");
      index.classList.add("shopping__item-cell", "shopping__item-index");
      deleteButton.classList.add("shopping__item-btn");
      btnContainer.classList.add("shopping__item-container", "shopping__item-cell");
      btnContent.classList.add("shopping__item-btn_content");
      deleteButton.dataset.index = `${i}`;
      index.textContent = `${i + 1}`;
      li.append(index);
      for (let key of Object.keys(tableItem)) {
        let div = document.createElement("div");
        div.classList.add("shopping__item-cell", `shopping__item-${key}`);
        if (key === "quantity") {
          div.textContent = `${
            tableItem[key] !== "" ? tableItem[key] + " шт." : ""
          }`;
        } else if (key === "price") {
          div.textContent = `${
            tableItem[key] !== "" ? tableItem[key] + " руб." : ""
          }`;
        } else {
          div.textContent = `${tableItem[key]}`;
        }
        li.append(div);
      }
      deleteButton.addEventListener("click", (event) => {
        const listIndex = event.currentTarget.dataset.index;
        shoppingList.splice(listIndex, 1);
        writeToLocalstorage(shoppingList);
        createListItems(shoppingList);
      });
      deleteButton.append(btnContent);
      btnContainer.append(deleteButton);
      li.append(btnContainer);
      document.querySelector(".shopping__list").append(li);
    }
  }

  // Очистка полей ввода

  function clearInputFields() {
    document.querySelectorAll(".form__input").forEach((inputItem) => {
      inputItem.value = "";
    });
  }

  // После загрузки страницы

  if (shoppingList !== null) {
    createListItems(shoppingList);
  } else {
    tableHeader();
  }

  // Добавление элемента в список

  document.querySelector(".form__btn").addEventListener("click", function () {
    try {
      if (fieldName.value === "") {
        throw new Error("Не заполнено поле 'Название'");
      } else {
        if (shoppingList === null) {
          shoppingList = [
            {
              name: fieldName.value,
              quantity: fieldQuantity.value,
              price: fieldPrice.value,
              date: fieldDate.value,
            },
          ];
          writeToLocalstorage(shoppingList);
          clearInputFields();
          createListItems(shoppingList);
        } else if (compareNameInShoppingList(shoppingList)) {
          shoppingList.push({
            name: fieldName.value,
            quantity: fieldQuantity.value,
            price: fieldPrice.value,
            date: fieldDate.value,
          });
          writeToLocalstorage(shoppingList);
          clearInputFields();
          createListItems(shoppingList);
        } else {
          fieldName.value = "";
          alert("Нельзя добавлять дубликат");
        }
      }
    } catch (e) {
      alert(e.message);
    }

    createListItems(shoppingList);
  });

  // Отображение способа фильтрации

  let filterSelect = document.querySelector(".filter__select");
  document
    .querySelector(`.filter__${filterSelect.value}`)
    .classList.add("show");
  document
    .querySelector(".filter__select")
    .addEventListener("change", (option) => {
      document.querySelectorAll(".filter__hide").forEach((item) => {
        item.classList.remove("show");
      });
      document
        .querySelector(`.filter__${option.currentTarget.value}`)
        .classList.add("show");
    });

  // Фитрация списка по имени

  function filterByName(param) {
    if (param) {
      let result = shoppingList.filter((item) => {
        return item.name.indexOf(param) !== -1;
      });
      createListItems(result);
    } else {
      createListItems(shoppingList);
      alert("Параметры фильтра не введены");
    }
  }

  // Фитрация списка по стоимости

  function filterByPrice(minPrice, maxPrice) {
    if (minPrice && maxPrice) {
      let result = shoppingList.filter(
        (item) => item.price >= minPrice && item.price <= maxPrice
      );
      createListItems(result);
    } else {
      alert("Параметры фильтра не введены");
    }
  }

  // Сортировка списка по имени

  function sortingByValue(key) {
    let result = shoppingList.sort((item1, item2) => {
      if (item1[key] > item2[key]) return 1;
      if (item1[key] === item2[key]) return 0;
      if (item1[key] < item2[key]) return -1;
    });
    createListItems(result);
  }

  // Фильтрация

  document.querySelector(".filter__btn").addEventListener("click", () => {
    if (document.querySelector(".filter__select").value === "name") {
      let nameValue = document.querySelector("#filterByName").value;
      filterByName(nameValue);
    } else {
      let minPrice = document.querySelector("#filterByPriceMin").value;
      let maxPrice = document.querySelector("#filterByPriceMax").value;
      filterByPrice(minPrice, maxPrice);
    }
  });

  // Сортировка

  document.querySelector(".sorting__btn").addEventListener("click", () => {
    document.querySelectorAll("[name=sortingRadio]").forEach((item) => {
      if (item.checked == true) {
        sortingByValue(item.value);
      }
    });
  });

  // Удаление элемента
});
